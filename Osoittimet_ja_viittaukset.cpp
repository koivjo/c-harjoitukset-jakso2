/*************************************
Tekij�: Joni Koivula
Jakson numero: 2
Jakson teht�v�numero: Osoittimet ja viittaukset
P�iv�ys: 26.5.2016
*************************************/
/*
// Harjoitus 9
#include <iostream>
using namespace std;

int main() {

	// Esittele muuttuja, johon voidaan tallentaa desimaalilukuja ja vastaava osoitin.
	double luku = 2.55;

	double *os_luku = &luku;

	return 0;
}
*/

/*
// Harjoitus 13
#include <iostream>
using namespace std;

int main() {

	// Esittele merkkimuuttuja ja viittaus. Sijoita viittauksen avulla merkkimuuttujaan jokin toinen merkki.
	char merkki = 'b';

	char *os_merkki = &merkki;

	*os_merkki = 'g';

	cout << merkki;

	return 0;
}
*/