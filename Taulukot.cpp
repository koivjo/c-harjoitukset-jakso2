/*************************************
Tekij�: Joni Koivula
Jakson numero: 2
Jakson teht�v�numero: Taulukot
P�iv�ys: 25.5.2016
*************************************/
/*
// Harjoitus 2
#include <iostream>
using namespace std;

int main() {

	// Ohjelma hakee taulukon vaihteluv�lin (suurin � pienin).

	short taulukko[6] = {22, 3, 15, 19, 1, 10};

	short min = taulukko[0];
	short max = taulukko[0];

	for (int i = 0; i < 6; i++) {

		if (min > taulukko[i]) min = taulukko[i];
		if (max < taulukko[i]) max = taulukko[i];

	}

	cout << "Pienin luku: " << min;
	cout << "\nSuurin luku: " << max;

	return 0;
}
*/
/*
// Harjoitus 5
#include <iostream>
using namespace std;

int main() {

	// Merkkitaulukossa on kaupungin nimi. Ohjelma laskee nimess� olevien vokaalien lukum��r�n.

	char kaupunki[] = {'R', 'a', 'u', 'm', 'a'};
	char vokaalit[] = {'a', 'e', 'i', 'o', 'u', 'y'};
	short vokaaliMaara = 0;

	for (int i = 0; i < sizeof(kaupunki); i++) {

		for (int j = 0; j < sizeof(vokaalit); j++) {
			if (kaupunki[i] == vokaalit[j]) vokaaliMaara++;
		}

	}

	cout << "Vokaalien maara: " << vokaaliMaara;

	return 0;
}
*/
/*
// Harjoitus 6
#include <iostream>
using namespace std;

int main() {

	// Ohjelma laskee merkkijonon pituuden.

	char merkkijono[] = {'m', 'e', 'r', 'k', 'k', 'i', '\0'};
	short pituus = 0;

	while (merkkijono[pituus] != '\0') {
		pituus++;
	}

	cout << "Merkkijonon '" << merkkijono << "' pituus: " << pituus << endl;

	// Tai k�ytt�m�ll� strlen-metodia

	cout << "Merkkijonon '" << merkkijono << "' pituus: " << strlen(merkkijono) << endl;

	return 0;
}
*/
/*
// Harjoitus 8
#include <iostream>
using namespace std;

int main() {

	// Ohjelma etsii merkkijonosta tietty� merkki�.

	char merkkijono[7];
	merkkijono[0] = 'K'; merkkijono[1] = 'u'; merkkijono[2] = 'u'; merkkijono[3] = 's';
	merkkijono[4] = 'a'; merkkijono[5] = 'm'; merkkijono[7] = 'o';

	for (int i = 0; i < strlen(merkkijono); i++) {

		if (merkkijono[i] == 'a') {
			cout << "Merkki '" << merkkijono[i] << "' loytyi.";
			break;
		}
	}

	return 0;
}
*/