/*************************************
Tekij�: Joni Koivula
Jakson numero: 2
Jakson teht�v�numero: Funktiot
P�iv�ys: 26.5.2016
*************************************/
/*
// Harjoitus 15
#include <iostream>
using namespace std;

int laskeKeskiarvo(int x, int y);

int main() {

	// Toteuta ohjelma, jossa funktio palauttaa kahden kokonaisluvun keskiarvon.
	int luku1 = 22;
	int luku2 = 8;

	cout << "Lukujen keskiarvo: " << laskeKeskiarvo(luku1, luku2) << endl;

	return 0;
}

int laskeKeskiarvo(int x, int y) {
	int keskiarvo = (x + y) / 2;
	return keskiarvo;
}
*/

/*
// Harjoitus 19
#include <iostream>
using namespace std;

int laskeKaanteisluku(int x);

int main() {

	// Toteuta funktio, joka palauttaa funktiolle viedyn kokonaisluvun k��nteisluvun.
	int luku1 = 10;

	cout << "Luvun kaanteisluku: " << laskeKaanteisluku(luku1) << endl;

	return 0;
}

int laskeKaanteisluku(int x) {
	int kaanteisluku = 1 / x;
	return kaanteisluku;
}
*/

/*
// Harjoitus 20
#include <iostream>
using namespace std;

void kerroLuvutKymmenella(double x, double y, double z);
void kerroLuvutKymmenella(double *x, double *y, double *z);

double luku1, luku2, luku3;

int main() {

	// Toteuta funktio, joka kertoo funktiolle viedyt 3 desimaalimuuttujaa pysyv�sti kymmenell�.
	// Tulosta my�s arvot ennen kutsua ja kutsun j�lkeen.

	// T�m� onnistuu k�ytt�en globaaleja muuttujia tai asettamalla funktion parametreiksi osoittimia -Joni

	luku1 = 1.25;
	luku2 = 22.6;
	luku3 = 15.15;

	cout << "Luvut ennen kerrontaa.\n";
	cout << "Luku1: " << luku1 << "\nLuku2: " << luku2 << "\nLuku3: " << luku3 << endl;

	kerroLuvutKymmenella(luku1, luku2, luku3);
	cout << "\nLuvut kerrottaan kymmenella, kayttaen globaaleja muuttujia.\n";

	cout << "Luku1: " << luku1 << "\nLuku2: " << luku2 << "\nLuku3: " << luku3 << endl;

	kerroLuvutKymmenella(&luku1, &luku2, &luku3);
	cout << "\nLuvut kerrottaan kymmenella, antamalla viittauksia parametreina.\n";

	cout << "Luku1: " << luku1 << "\nLuku2: " << luku2 << "\nLuku3: " << luku3 << endl;

	system("pause");
	return 0;
}

void kerroLuvutKymmenella(double x, double y, double z) {
	luku1 = x * 10;
	luku2 = y * 10;
	luku3 = z * 10;
}
void kerroLuvutKymmenella(double *x, double *y, double *z) {
	*x = *x * 10;
	*y = *y * 10;
	*z = *z * 10;
}
*/

/*
// Harjoitus 23
#include <iostream>
using namespace std;

void puolitaMuuttujat(double *x, double *y, double *z);
double laskeKeskiarvo(double x, double y, double z);
double * haeVaihteluvali(double x, double y, double z);

int main() {

	// Ohjelmassa on 3 paikallista desimaalilukumuuttujaa, jotka on alustettu joillakin arvoilla.
	// Ohjelman funktio(nro1) puolittaa nuo muuttujat.Toinen funktio(nro 2) palauttaa muuttujien keskiarvon.
	// Kolmas funktio(nro 3) sijoittaa nuo muuttujien arvot paikalliseen taulukkoonsa ja palauttaa vaihteluv�lin(suurin � pienin).
	double luku1, luku2, luku3;

	luku1 = 1.25;
	luku2 = 22.6;
	luku3 = 15.15;

	cout << "Luvut ennen puolittamista\nLuku1: " << luku1 << "\nLuku2: " << luku2 << "\nLuku3: " << luku3 << endl;
	puolitaMuuttujat(&luku1, &luku2, &luku3);
	cout << "\nLuvut puolittamisen j�lkeen\nLuku1: " << luku1 << "\nLuku2: " << luku2 << "\nLuku3: " << luku3 << endl;

	cout << "\nLukujen keskiarvo: " << laskeKeskiarvo(luku1, luku2, luku3) << endl;

	double *vaihteluvali;
	vaihteluvali = haeVaihteluvali(luku1, luku2, luku3);

	cout << "\nVaihteluvali\nSuurin: " << vaihteluvali[0] << "\nPienin: " << vaihteluvali[1] << endl;

	system("pause");
	return 0;

}

void puolitaMuuttujat(double *x, double *y, double *z) {
	*x = *x / 2;
	*y = *y / 2;
	*z = *z / 2;
}
double laskeKeskiarvo(double x, double y, double z) {
	double keskiarvo = (x + y + z) / 3;
	return keskiarvo;
}
double * haeVaihteluvali(double x, double y, double z) {
	double taulukko[] = { x, y, z };

	double suurin = taulukko[0];
	double pienin = taulukko[0];

	for (int i = 0; i < 3; i++) {
		if (suurin < taulukko[i]) suurin = taulukko[i];
		if (pienin > taulukko[i]) pienin = taulukko[i];
	}
	
	double vaihteluvali[] = { suurin, pienin };

	return vaihteluvali;
}
*/